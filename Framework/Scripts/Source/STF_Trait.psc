scriptname STF_Trait extends STF_TraitBase
{Trait subclass for Skyrim Character Traits}

; ============= Properties and class variables ================================

LeveledItem  property ItemPack  auto
LeveledSpell property SpellPack auto

; string property Description auto

; ============= Event handling ================================================

; ============= Utility functions =============================================

function Activate(Actor player)
	player.AddPerk(self)
	
	if(ItemPack as bool)
		int i = 0
		while(i < ItemPack.GetNumForms())
			player.AddItem(ItemPack.GetNthForm(i), ItemPack.GetNthCount(i), true) ; Add item to player's inventory silently
			i += 1
		endwhile
	endif
	
	if(SpellPack)
		int i = 0
		while(i < SpellPack.GetNumForms())
			player.AddSpell(SpellPack.GetNthForm(i) as Spell, false) ; Add spell to player silently
			i += 1
		endwhile
	endif
endfunction
