scriptname STF_TraitBase extends Perk
{Base class for Skyrim Character Traits.
All trait types must be subclasses of this class.}

; ============= Properties and class variables ================================

STF_Trait[] property BlockingTraits   auto
Race[]      property BlockingRaces    auto

bool        property Selected = false auto hidden

; ============= Event handling ================================================

event OnInit()
	RegisterForMenu("RaceSex Menu")
endevent

event OnMenuClose(string menuName)
	if(menuName == "RaceSex Menu")
		Race playerRace = Game.GetPlayer().GetRace()
		int i = 0
		while(i < BlockingRaces.Length)
			if(playerRace == BlockingRaces[i])
				GotoState("RaceBlocked")
				return ; Player race is not compatible with trait
			else
				i += 1
			endif
		endwhile
		GotoState("")
	endif
endevent

; ============= Utility functions =============================================

bool function IsBlocked()
	int i = 0
	; Check if player has blocking traits
	while(i < BlockingTraits.Length)
		if(BlockingTraits[i].Selected)
			return true ; Trait is blocked by other active trait
		else
			i += 1
		endif
	endwhile
	
	return false ; Trait is available
endfunction

function Activate(Actor player)
{This function must be overridden in subclasses}
endfunction

; ============= Custom states =================================================

state RaceBlocked ; Player race is not compatible with trait
	bool function IsBlocked()
		return true
	endfunction
endstate

state Activated ; Default state for activated traits
	function Activate(Actor a)
	endfunction
endstate
