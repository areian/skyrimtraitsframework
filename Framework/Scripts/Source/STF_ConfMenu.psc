scriptname STF_ConfMenu extends SKI_ConfigBase  
{SkyTraits Framework MCM integration}

; ============= Properties and class variables ================================

Actor              property PlayerRef auto hidden
STF_TraitPackage[] property Packages  auto hidden
String[]           property Names     auto hidden

STF_TraitPackage            currentPackage

int[] _oids1
int[] _oids2

; ============= Event handling ================================================

event OnConfigInit()
	OnGameReload()
endevent

event OnGameReload()
endevent

event OnMenuClose(string menuName)
endevent

event OnPageReset(string page)
endevent

; ============= Utility functions =============================================


; ============= Custom states =================================================
